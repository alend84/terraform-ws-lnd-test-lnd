module "svc_tst_svc" {
  source  = "app.terraform.io/ffn/service/google"
  version = "~> 0.7.1"

  context = module.base.contexts.default

  svc_name = "Test service"
  svc_key  = "tst-svc"

  pagerduty_escalation_policy_id = "PGHCY37"
}