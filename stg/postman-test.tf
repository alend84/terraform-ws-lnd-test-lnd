module "svc_postman_test" {
  source  = "app.terraform.io/ffn/service/google"
  version = "~> 0.7.5"

  context = module.base.contexts.default

  svc_name = "Postman Test"
  svc_key  = "postman-test"

  pagerduty_escalation_policy_id = "PGHCY37"
}
