terraform {
  backend "remote" {
    organization = "ffn"
    workspaces {
      name = "lnd-test-lnd-dev"
    }
  }
}

provider "google" {
  region = "us-central1"
}

provider "google-beta" {
  region = "us-central1"
}

module "base" {
  source  = "app.terraform.io/ffn/remote-state/terraform"
  version = "~> 0.2.8"

  workspace = {
    name      = "lnd-app-test-lnd"
    child_key = "dev"
  }
  additional_workspaces = {
    business_unit = {
      name      = "lnd-dev"
      child_key = "default"
    }
  }
}
